
# The Talk Project #

## About ##

This will be a social application where the content cannot be removed and voting is the final say of where things go. Admin has a "mega vote" where it's just a vote with priority. This is in it's beginning stages so expect some rough spots.

## Prerequisites ##

- Python 2.6 or 2.7
- pip
- virtualenv (virtualenvwrapper is recommended for use during development)

## Installation ##

I hope you're a developer in you're going to run this yet.


License
-------
This software is licensed under the [New BSD License][BSD]. For more
information, read the file ``LICENSE``.

[BSD]: http://opensource.org/licenses/BSD-3-Clause
