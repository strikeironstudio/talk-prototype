""" Default urlconf for talk """
from __future__ import unicode_literals

from django.conf.urls import include, patterns, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
admin.autodiscover()

from views import LinkList, LinkCreate, LinkDetail, CommentList, UserDetailView, MyUserDetailView, TagList

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'talk.views.home', name='home'),
    # url(r'^u/', include('users.urls')),
    url("^x/$", LinkList.as_view(), name="home"),
    url("^x/newest/$", LinkList.as_view(), {"by_score": False}, name="link_list_latest"),
	url("^x/link/create/$", login_required(LinkCreate.as_view()), name="link_create"),
    url("^x/link/(?P<slug>[^/]+)/$", LinkDetail.as_view(), name="link_detail"),
	url("^x/tags/$", TagList.as_view(), name="tag_list"),
    url("^x/tags/(?P<tag>[^/]+)/$", LinkList.as_view(), name="link_list_tag"),
    url("^p/$", login_required(MyUserDetailView.as_view()), name='my_profile'),
    url("^p/(?P<username>[^/]+)/$", login_required(UserDetailView.as_view()), name="profile"),
    url("^p/(?P<username>[^/]+)/links/$", login_required(LinkList.as_view()), {"by_score": False}, name="link_list_user"),
    url("^p/(?P<username>[^/]+)/comments/$", login_required(CommentList.as_view()), {"by_score": False}, name="comment_list_user"),
    url("^a/", include('allauth.urls')),
)
