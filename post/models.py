from django.conf import settings
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from mezzanine.core.models import Displayable, Ownable
from mezzanine.generic.fields import CommentsField, RatingField
from mezzanine.core.request import current_request
from allauth.socialaccount.models import SocialAccount
import hashlib

USER_MODEL = getattr(settings, 'AUTH_USER_MODEL', 'auth.User')


try:
    from urllib.parse import urlparse
except ImportError:
    from urlparse import urlparse

class Link(Displayable, Ownable):
    link = models.URLField()
    comments = CommentsField()
    rating = RatingField()

    def get_absolute_url(self):
        return reverse("link_detail", kwargs={"slug": self.slug})

    @property
    def domain(self):
        return urlparse(self.url).netloc

    @property
    def url(self):
        if self.link:
            return self.link
        return current_request().build_absolute_uri(self.get_absolute_url())


class Profile(Displayable):
    user = models.OneToOneField(USER_MODEL)
    website = models.URLField(blank=True)
    bio = models.TextField(blank=True)
    karma = models.IntegerField(default=0, editable=False)

    def __unicode__(self):
        return "%s (%s)" % (self.user, self.karma)

    def profile_image_url(self):
        fb_uid = SocialAccount.objects.filter(user_id=self.user.id, provider='facebook')
     
        if len(fb_uid):
            return "http://graph.facebook.com/{}/picture?width=40&height=40&d=identicon".format(fb_uid[0].uid)
        if len(self.user.email):
            return "http://www.gravatar.com/avatar/{}?s=40&d=identicon".format(hashlib.md5(self.user.email).hexdigest())
        return "http://www.gravatar.com/avatar?s=40"
 
User.profile = property(lambda u: Profile.objects.get_or_create(user=u)[0])
