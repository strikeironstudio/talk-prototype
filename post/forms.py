from django import forms
from django.conf import settings
from django.forms.models import modelform_factory
from django.forms import ValidationError
from django.shortcuts import render_to_response
from django.template import RequestContext
from allauth.account.forms import LoginForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field
from crispy_forms.bootstrap import FormActions 
from .models import Link

BaseLinkForm = modelform_factory(Link, fields=["title", "link", "description"])

class LinkForm(BaseLinkForm):
    def clean(self):
        link = self.cleaned_data.get("link", None)
        description = self.cleaned_data.get("description", None)
        if not link and not description:
            raise ValidationError("Either a link or description is required")
        return self.cleaned_data

class MyLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        super(MyLoginForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()

        # You don't want the `remember` field?
        #if 'remember' in self.fields.keys():
        #    del self.fields['remember']

        helper = FormHelper()
        helper.form_show_labels = False
        helper.layout = Layout(
            Field('login', placeholder = 'Email address'),
            Field('password', placeholder = 'Password'),
            FormActions(
                Submit('submit', 'Log In', css_class = 'btn-primary')
            ),
        )
        self.helper = helper

class LoginFormMiddleware(object):
    def process_request(self, request):
        # if the top login form has been posted
        if request.method == 'POST':
            # validate the form
            form = MyLoginForm(prefix="login", data=request.POST)
            if form.is_valid():
                # log the user in
                form.login(request)
                # if this is the logout page, then redirect to /
                # so we don't get logged out just after logging in
                if '/account/logout/' in request.get_full_path():
                    return HttpResponseRedirect('/')
        else:
            form = MyLoginForm(prefix="login")
        # attach the form to the request so it can be accessed within the templates
        request.login_form = form
